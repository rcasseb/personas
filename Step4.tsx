import { useState } from 'react';
import {View , Text, Button} from 'react-native'
import {Checkbox} from 'react-native-paper'
import IStep from './IStep';

export default function Step4({ backStep }: IStep) {
  const [lider, setLider] = useState<boolean>(false);
  const [comunicativo, setComunicativo] = useState<boolean>(false);
  const [colaborativo, setColaborativo] = useState<boolean>(false);  

  return(
    <View>
      <Text>
        Habilidades
      </Text>
      <View>
      <Checkbox.Item label="Lider" status={lider?'checked':'unchecked'} onPress={()=>{setLider(!lider)}} />
      <Checkbox.Item label="Comunicativo" status={comunicativo?'checked':'unchecked'} onPress={()=>{setComunicativo(!comunicativo)}} />
      <Checkbox.Item label="Colaborativo" status={colaborativo?'checked':'unchecked'} onPress={()=>{setColaborativo(!colaborativo)}} />
      </View>
      
      <Button 
        title='BACK'
        onPress={() => { backStep() }}
      />
    </View>
  );

}