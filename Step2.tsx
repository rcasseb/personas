import { useState } from 'react';
import {View , Text, Button} from 'react-native'
import { TextInput, } from 'react-native-paper'
import IStep from './IStep';

export default function Step2({ nextStep, backStep }: IStep) {
  const [email, setEmail] = useState<string>();
  const [whatsapp, setWhatapp] = useState<string>();

  return(
    <View>
      <Text>
        Dados Pessoais
      </Text>
      <View>
        <TextInput
          label="E-mail"
          mode='outlined'
          activeOutlineColor='#0000ff'
          onChangeText={ t => {setEmail(t)} }
        />
        
        <TextInput
          label="Whatsapp"
          mode='outlined'
          activeOutlineColor='#0000ff'
          onChangeText={ t => {setWhatapp(t)} }
        />
      </View>
      <Button 
        title='NEXT'
        onPress={() => { nextStep({'email': email, 'whatsapp': whatsapp}) }}
      />
      <Button 
        title='BACK'
        onPress={() => { backStep() }}
      />
    </View>
  );

}