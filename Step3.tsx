import {View , Text, Button} from 'react-native'
import { TextInput, } from 'react-native-paper'
import IStep from './IStep';

export default function Step3({ nextStep, backStep }: IStep) {
  
  return(
    <View>
      <Text>
        Poscionamento
      </Text>
      <View>
        <TextInput
          label="Cargo"
          mode='outlined'
          activeOutlineColor='#0000ff'
        />
      </View>
      <Button 
        title='NEXT'
        onPress={() => { nextStep() }}
      />
      <Button 
        title='BACK'
        onPress={() => { backStep() }}
      />
    </View>
  );

}